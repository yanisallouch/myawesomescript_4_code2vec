private boolean isJavaFile(File file) {

  final String extentionWanted = ".java";
  int extentionIndex = file.getName().length() - 5;
  int endFileIndex = file.getName().length();
  final String fileExtention = file.getName().substring(extentionIndex, endFileIndex);

  return fileExtention.equals(extentionWanted);
}