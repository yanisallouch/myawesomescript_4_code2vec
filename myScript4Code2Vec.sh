export CODE2VEC_PATH=code2vec.py
export MODELS_PATH=models/java14_model/saved_model_iter8.release
export RAW_OUTPUT_PATH=output.out
export ERR_LOG_PATH=err.log
export OUTPUT_PATH=ast_path.out
export TIME2SLEEP_IN_SECONDS=60

echo "Working"
echo -ne '\n' | python3 $CODE2VEC_PATH --load $MODELS_PATH --predict > $RAW_OUTPUT_PATH 2> $ERR_LOG_PATH &
echo "Waiting for code2vec..." && sleep $TIME2SLEEP_IN_SECONDS
echo "Code2Vec has Finished!"
echo "Processing " $RAW_OUTPUT_PATH
grep -E "context:|Attention:|Original name:" $RAW_OUTPUT_PATH > $OUTPUT_PATH
echo "Saved result in " $OUTPUT_PATH
echo "Done."