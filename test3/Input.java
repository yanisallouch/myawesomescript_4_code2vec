public class MyApplicationForAddition {

        public static void main(String[] args) {
                int a = getA();
                int b = getB();

                int result = getResultAddition(a,b);

                System.out.println("Le résultat est " + result);        
        }

        int getA(){
                return 40;
        }

        int getB(){
                return 2;
        }

        int getResultAddition(int n, int m){
                return n + m;
        }
}